'use strict'

const mongoose = require('mongoose')
const schema = mongoose.Schema

const messageSchema = new Schema({
    name: { type: string, required: true},
    body: { type: string, required: true},
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
})

const messagesModel = mongoose.model('messages', messageSchema)

module.exports = messagesModel